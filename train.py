import os
from pathlib import Path
from datetime import datetime
import shutil

import yaml
from easydict import EasyDict
import tensorflow as tf

from tensorforce.environments import Environment
from tensorforce.agents import Agent
from tensorforce.execution import Runner


class Trainer:
    def __init__(self):
        with open('config.yaml') as file:
            self.config = EasyDict(yaml.safe_load(file))
        self.experiment_path = Path('experiments', datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
        os.makedirs(self.experiment_path)
        shutil.copy2('config.yaml', self.experiment_path)

        self.environment = Environment.create(**self.config.environment)

        saver = str(self.experiment_path)
        summarizer = {
            'directory': str(self.experiment_path),
            'filename': 'logs'
        }

        self.agent = Agent.create(
            environment=self.environment,
            saver=saver,
            summarizer=summarizer,
            **self.config.agent
        )

    def train(self):
        file_writer = tf.summary.create_file_writer(str(self.experiment_path / 'config'))
        with file_writer.as_default():
            with open('config.yaml') as file:
                config_text = ''.join('\t' + line for line in file.read().splitlines(True))
                tf.summary.text('config', config_text, step=0)
        file_writer.flush()

        runner = Runner(self.agent, self.environment)
        runner.run(self.config.train.train_episodes)
        runner.run(self.config.train.evaluation_episodes, evaluation=True)
        runner.close()


if __name__ == '__main__':
    trainer = Trainer()
    trainer.train()
