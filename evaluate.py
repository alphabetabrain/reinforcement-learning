import yaml
from easydict import EasyDict

from tensorforce.environments import Environment
from tensorforce.agents import Agent
from tensorforce.execution import Runner


class Evaluator:
    def __init__(self):
        with open('config.yaml') as file:
            self.config = EasyDict(yaml.safe_load(file))

        self.environment = Environment.create(
            visualize=self.config.evaluate.visualize,
            **self.config.environment
        )

        if self.config.evaluate.experiment is not None:
            self.agent = Agent.load(
                directory=self.config.evaluate.experiment,
                environment=self.environment,
                **self.config.agent
            )
        else:
            self.agent = Agent.create(
                agent='random',
                environment=self.environment
            )

    def evaluate(self):
        runner = Runner(self.agent, self.environment)
        runner.run(self.config.evaluate.evaluation_episodes, evaluation=True)
        runner.close()


if __name__ == '__main__':
    evaluator = Evaluator()
    evaluator.evaluate()
